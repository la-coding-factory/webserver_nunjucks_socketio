const http = require('http')
const express = require('express')
const nunjucks = require('nunjucks')
const { Server } = require('socket.io')
const router = require('./router')

/* Initialiser l'instance Express */
const app = express()
/* Creer le serveur HTTP */
const server = http.createServer(app)
/* Création de l'instance socket.io et 'hook' au serveur HTTP */
const io = new Server(server)

/* Quand le serveur reçoit une connexion client, l'event est déclenché */
io.on('connection', (socketClient) => {
	console.log(socketClient.id)
	/* Quand le client envoi "event-name" le callback associé est déclenché */
	socketClient.on('event-name', (data) => {
		console.log(socketClient.id, 'sent "event-name" with data:', data)
	})
})

nunjucks.configure('views', {
	autoescape: true,
	express: app,
})

app.use(express.static('public'))

app.use('/pages', router)

server.listen(8081, () => {
	console.log('Listening on 8081')
})
