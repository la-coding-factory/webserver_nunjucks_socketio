const Router = require('express').Router

const router = Router()

router.get('/', async (req, res) => {
	res.render('home.html', {
		title: 'Home page'
	})

})

module.exports = router
