# Installer Socket.io

```
npm i --save socket.io
```

## Ajouter Socket.io au serveur web

Création du serveur Socket.io
```js
const express = require('express');
const app = express();
const http = require('http');
// Serveur HTTP
const server = http.createServer(app);

const { Server } = require('socket.io')
const io = new Server(server);
```


Gestion des connexions client

```js

io.on('connection', socketClient => {
	console.log('Un client est connecté', socketClient.id)

	socketClient.on('event-name', data => {
		console.log(socketClient.id, 'event-name', data)
	})

})
```

Il est également possible de
Séparer le code en plusieurs fichiers, ou de passer des fonctions (rappel: un callback est une fonction)

```js
function connectionHandler(socketClient) {
	console.log('Un client est connecté', socketClient.id)

	// socketClient.on(...)
}

io.on('connection', connectionHandler)
```
